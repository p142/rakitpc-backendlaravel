<?php

namespace App\Filament\Resources\CasingResource\Pages;

use App\Filament\Resources\CasingResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCasing extends CreateRecord
{
    protected static string $resource = CasingResource::class;

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
