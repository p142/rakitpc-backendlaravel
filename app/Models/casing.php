<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class casing extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'casing';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idCasing';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaCasing', 'MerkCasing', 'MoboCompatible', 'DrivebayCasing', 'FanSupport', 'FrontPanel', 'DimensionCasing', 'WeightCasing', 'ColorCasing', 'MaxVgaLength', 'MaxCoolerHeight', 'MaxPSU', 'CasingSidePanel', 'Harga', 'idToko', 'Garansi', 'Rangking', 'ImageLink', 'Links'];

    public function toko()
    {
        return $this->belongsTo(ListToko::class, 'idToko');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
