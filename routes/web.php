<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V2\v2BuildsController;
use App\Http\Controllers\V2\v2CpuController;
use App\Http\Controllers\V2\v2FanController;
use App\Http\Controllers\V2\v2CasingController;
use App\Http\Controllers\V2\v2CpuCoolerController;
use App\Http\Controllers\V2\v2KeyboardController;
use App\Http\Controllers\V2\v2MonitorController;
use App\Http\Controllers\V2\v2MotherboardController;
use App\Http\Controllers\V2\v2MouseController;
use App\Http\Controllers\V2\v2PowerSupplyController;
use App\Http\Controllers\V2\v2RamController;
use App\Http\Controllers\V2\v2StorageController;
use App\Http\Controllers\V2\v2VgaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
