<?php

namespace App\Filament\Resources\CasingResource\Pages;

use App\Filament\Resources\CasingResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCasing extends EditRecord
{
    protected static string $resource = CasingResource::class;

    protected function getActions(): array
    {
        return [
            Actions\ReplicateAction::make()->label('Copy'),
            Actions\DeleteAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
