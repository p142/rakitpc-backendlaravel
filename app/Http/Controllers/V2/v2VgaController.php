<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\vga;

class v2VgaController extends Controller
{
    public function getvga(){
        $hasil =  vga::select("*")
            ->where('Rangking', 1)
            ->get();
        return $hasil;
    }

    public function getvgaID($id){
        $hasil =  vga::select("*")
                        ->where('idvga', $id)
                        ->get();
        return $hasil;
    }

    public function PostvgaFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = vga::select("*")
                        ->orderBy("Namavga")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = vga::select("*")
                        ->orderByDesc("Namavga")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = vga::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "AMD") {
            $hasil = vga::select("*")
                        ->where('Generation','like','%AMD%')
                        ->get(); 
          }
          elseif ($Request->Request == "NVIDIA") {
            $hasil = vga::select("*")
                        ->where('Namavga','like','%tx%')
                        ->get(); 
          }
          elseif ($Request->Request == "RTX") {
            $hasil = vga::select("*")
                        ->where('RTcores','like','%0%')
                        ->get(); 
          }
          elseif ($Request->Request == "Vramlebih4GB") {
            $hasil = vga::select("*")
                        ->where('Memoryvga','like','%0%')
                        ->get(); 
          }
          else {
            $hasil = $hasil =  vga::select("*")
                    ->where('Rangking', 1)
                    ->get();
          }
          return $hasil;
    }
}
