<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Create a CPU</h1>
 
    <form method="post" action="{{route('cpu.store')}}">
        @csrf 
        @method('post')
        <div>
            <label>NamaCPU</label>
            <input type="text" name="NamaCPU" placeholder="Name" />
        </div>
        <div>
            <label>MerkCPU</label>
            <input type="text" name="MerkCPU" placeholder="Qty" />
        </div>
        <div>
            <label>Socket</label>
            <input type="text" name="Socket" placeholder="Price" />
        </div>
        <div>
            <label>CoreCount</label>
            <input type="number" name="CoreCount" placeholder="Description" />
        </div>
        <div>
            <label>ThreadsCount</label>
            <input type="number" name="ThreadsCount" placeholder="Description" />
        </div>
        <div>
            <label>BaseClock</label>
            <input type="number" name="BaseClock" placeholder="Description" />
        </div>
        <div>
            <label>MaxClock</label>
            <input type="number" name="MaxClock" placeholder="Description" />
        </div>
        <div>
            <label>DefaultTDP</label>
            <input type="number" name="DefaultTDP" placeholder="Description" />
        </div>
        <div>
            <label>LaunchDate</label>
            <input type="text" name="LaunchDate" placeholder="Description" />
        </div>
        <div>
            <label>Cache</label>
            <input type="text" name="Cache" placeholder="Description" />
        </div>
        <div>
            <label>Unlocked</label>
            <input type="text" name="Unlocked" placeholder="Description" />
        </div>
        <div>
            <label>MaxTemp</label>
            <input type="text" name="MaxTemp" placeholder="Description" />
        </div> 
        <div>
            <label>ProcTechnology</label>
            <input type="text" name="ProcTechnology" placeholder="Description" />
        </div>
        <div>
            <label>Toko</label>
            <input type="number" name="Toko" placeholder="Description" />
        </div>
        <div>
            <label>Garansi</label>
            <input type="text" name="Garansi" placeholder="Description" />
        </div>
        <div>
            <label>Harga</label>
            <input type="number" name="Harga" placeholder="Description" />
        </div>
        <div>
            <label>Rangking</label>
            <input type="number" name="Rangking" placeholder="Description" />
        </div>
        <div>
            <label>ImageLink</label>
            <input type="text" name="ImageLink" placeholder="Description" />
        </div>
        <div>
            <label>Links</label>
            <input type="text" name="Links" placeholder="Description" />
        </div>
        <div>
            <input type="submit" value="Save a New Product" />
        </div>
    </form>
</body>
</html>