<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class monitor extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'monitor';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMonitor';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaMonitor', 'MerkMonitor', 'MonitorResolusi', 'MonitorRefreshRate', 'MonitorAspectRatio', 'MonitorPort', 'MonitorSize', 'Ultrawide', 'PanelType', 'HDR', 'ScreenTechnology', 'idToko', 'Garansi', 'Harga', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
