<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ram;

class v2RamController extends Controller
{
    public function getRam(){
        $hasil =  ram::select("*")
              ->where('Rangking', 1)
              ->get();
  
        return $hasil;
    }

    public function getRamID($id){
        $hasil =  ram::select("*")
                        ->where('idRam', $id)
                        ->get();
        return $hasil;
    }

    public function PostRamFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = ram::select("*")
                        ->orderBy("NamaRam")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = Ram::select("*")
                        ->orderByDesc("NamaRam")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = Ram::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = Ram::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "MemoryTinggi") {
            $hasil = Ram::select("*")
                        ->orderByDesc("MemorySize")
                        ->get();
          }
          elseif ($Request->Request == "MemoryRendah") {
            $hasil = Ram::select("*")
                        ->orderBy("MemorySize")
                        ->get();
          }
          elseif ($Request->Request == "SpeedTinggi") {
            $hasil = Ram::select("*")
                        ->orderByDesc("MemorySpeed")
                        ->get();
          }
          elseif ($Request->Request == "SpeedRendah") {
            $hasil = Ram::select("*")
                        ->orderBy("MemorySpeed")
                        ->get();
          }else {
            $hasil = $hasil =  ram::select("*")
                    ->where('Rangking', 1)
                    ->get();
          }
          
          return $hasil;
    }
}
