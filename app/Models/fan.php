<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class fan extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'fan';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idFans';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaFans', 'MerkFans', 'SizeFans', 'VoltageFans', 'PowerFans', 'SpeedFans', 'ColorFans', 'RGB', 'idToko', 'Garansi', 'Harga', 'ImageLinks', 'PowerConnector', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
