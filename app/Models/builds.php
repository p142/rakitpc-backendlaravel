<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\motherboard;

class builds extends Model
{
     /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idBuilds';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['KebutuhanBuilds', 'BudgetBuilds', 'KebutuhanStorage', 'Motherboard', 'NamaMobo', 'HargaMobo', 'ImgMobo', 'Cpu', 'NamaCpu', 'HargaCpu', 'ImgCpu', 'Ram', 'NamaRam', 'HargaRam', 'ImgRam', 'VGA', 'NamaVga', 'HargaVga', 'ImgVga', 'PSU', 'NamaPsu', 'HargaPsu', 'ImgPsu', 'CpuCooler', 'NamaCpuCooler', 'HargaCpuCooler', 'ImgCpuCooler', 'Storage', 'NamaStorage', 'HargaStorage', 'ImgStorage', 'Storage2', 'NamaStorage2', 'HargaStorage2', 'ImgStorage2', 'Fans', 'NamaFans', 'HargaFans', 'ImgFans', 'Casing', 'NamaCasing', 'HargaCasing', 'ImgCasing', 'ImgLinks', 'HargaBuilds', 'Links'];

}
