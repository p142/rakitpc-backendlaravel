<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class ram extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ram';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idRam';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaRam', 'MerkRam', 'MemoryType', 'MemorySize', 'MemorySpeed', 'LatencyCL', 'Voltage', 'HeatSpreader', 'Color', 'RGB', 'idToko', 'Garansi', 'Harga', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
