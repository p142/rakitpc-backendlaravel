<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\casing;

class CasingController extends Controller
{
    public function getCasing(){
      $hasil = casing::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }

    public function getCasingID($id){
        $hasil =  casing::select("*")
                        ->where('idCasing', $id)
                        ->get();
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);                
        return $hasil;
    }

    public function postCasingFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = casing::select("*")
                        ->orderBy("NamaCasing")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = casing::select("*")
                        ->orderByDesc("NamaCasing")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = casing::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = casing::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "ATX") {
            $hasil = casing::select("*")
                        ->where('MoboCompatible','like','%ATX%')
                        ->get();
          }
          elseif ($Request->Request == "Mini") {
            $hasil = casing::select("*")
                        ->where('MoboCompatible','like','%mini%')
                        ->get();
          }
          elseif ($Request->Request == "Micro") {
            $hasil = casing::select("*")
                        ->where('MoboCompatible','like','%micro%')
                        ->get();
          }elseif ($Request->Request == "White") {
            $hasil = casing::select("*")
                        ->where('ColorCasing','like','%white%')
                        ->get();
          }elseif ($Request->Request == "Black") {
            $hasil = casing::select("*")
                        ->where('ColorCasing','like','%black%')
                        ->get();
          }else{
            $hasil = casing::all();
          }

        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }
    //
}
