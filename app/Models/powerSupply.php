<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class powerSupply extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'power_supply';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idPSU';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaPSU', 'MerkPSU', 'WattPSU', '80PlusEfficient', 'FormFactor', 'Modular', 'SataConnector', 'PCIEConnector', 'SilentMode', 'FanSize', 'ATXConnector', 'ColorPsu', 'RGB', 'idToko', 'Garansi', 'Harga', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
