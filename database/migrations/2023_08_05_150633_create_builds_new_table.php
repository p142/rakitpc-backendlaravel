<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builds_new', function (Blueprint $table) {
            $table->bigIncrements('idBuilds');
            $table->integer('KebutuhanBuilds');
            $table->integer('BudgetBuilds');
            $table->integer('KebutuhanStorage');
            $table->foreignId('idToko')->nullable()->default('0');
            $table->foreign('idToko')->references('idToko')->on('list_toko');
            $table->integer('Rangking');
            $table->string('Garansi', 200);
            $table->integer('Motherboard');
            $table->integer('Cpu');
            $table->integer('Ram');
            $table->integer('Vga');
            $table->integer('PSU');
            $table->integer('CpuCooler');
            $table->integer('Storage');
            $table->integer('Storage2')->default('0');
            $table->integer('Fans');
            $table->integer('Casing');
            $table->string('ImgLinks')->nullable()->default('NULL');
            $table->string('HargaBuilds')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builds_new');
    }
}
