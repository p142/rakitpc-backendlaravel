<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\cpu;

class v2CpuController extends Controller
{
    public function getCpu(){
        $hasil =  cpu::select("*")
              ->where('Rangking', 1)
              ->get();
        return $hasil;
    }

      public function getCpuID($id){
        $hasil =  cpu::select("*")
                        ->where('idCpu', $id)
                        ->get();
        return $hasil;
    }

    public function PostCpuFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = cpu::select("*")
                        ->orderBy("NamaCpu")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = cpu::select("*")
                        ->orderByDesc("NamaCpu")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = cpu::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif (($Request->Request) == "Mahal") {
            $hasil = cpu::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Intel") {
            $hasil = cpu::select("*")
                        ->where('MerkCPU','like','%intel%')
                        ->get();
          }
          elseif ($Request->Request == "AMD") {
            $hasil = cpu::select("*")
                        ->where('MerkCPU','like','%amd%')
                        ->get();
          }
          elseif ($Request->Request == "Core") {
            $hasil = cpu::select("*")
                        ->orderByDesc("CoreCount")
                        ->get();
          }
          elseif ($Request->Request == "Boost") {
            $hasil = cpu::select("*")
                        ->orderByDesc("MaxClock")
                        ->get();
          }
          elseif ($Request->Request == "Efisien") {
             $hasil = cpu::select("*")
                        ->orderBy("DefaultTDP")
                        ->get();
          }
          elseif ($Request->Request == "Unlocked") {
            $hasil = cpu::select("*")
                        ->where('Unlocked','like','%yes%')
                        ->get();
          }
          else {
            $hasil = cpu::select("*")
                ->where('Rangking', 1)
                ->get();
        }
        return $hasil;
    }
}
