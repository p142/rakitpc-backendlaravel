<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\powerSupply;


class PowerSupplyController extends Controller
{
    public function getPowerSupply(){
        $hasil = powerSupply::all();
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }

    public function getPowerSupplyID($id){
        $hasil =  powerSupply::select("*")
                        ->where('idPSU', $id)
                        ->get();
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }

    public function PostPowerSupplyFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = powerSupply::select("*")
                        ->orderBy("NamaPSU")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = powerSupply::select("*")
                        ->orderByDesc("NamaPSU")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = powerSupply::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = powerSupply::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "WattTinggi") {
            $hasil = powerSupply::select("*")
                        ->orderByDesc("WattPSU")
                        ->get();
          }
          elseif ($Request->Request == "WattRendah") {
            $hasil = powerSupply::select("*")
                        ->orderBy("WattPSU")
                        ->get();
          }
          elseif ($Request->Request == "White") {
            $hasil = powerSupply::select("*")
                        ->where('80PlusEfficient','like','%white%')
                        ->get(); 
          }
          elseif ($Request->Request == "Gold") {
            $hasil = powerSupply::select("*")
                        ->where('80PlusEfficient','like','%gold%')
                        ->get(); 
          }
          elseif ($Request->Request == "Bronze") {
            $hasil = powerSupply::select("*")
                        ->where('80PlusEfficient','like','%bronze%')
                        ->get(); 
          }
          elseif ($Request->Request == "Modular") {
            $hasil = powerSupply::select("*")
                        ->where('Modular','like','%modular%')
                        ->get(); 
          }
          elseif ($Request->Request == "NonModular") {
            $hasil = powerSupply::select("*")
                        ->where('Modular','like','%No%')
                        ->get(); 
          }else {
            $hasil = powerSupply::all();
          }
          $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
          return $hasil;
    }
}
