<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class cpu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cpu';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idCPU';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaCPU', 'MerkCPU', 'Socket', 'CoreCount', 'ThreadsCount', 'BaseClock', 'DefaultTDP', 'LaunchDate', 'Cache', 'MaxClock', 'Unlocked', 'MaxTemp', 'ProcTechnology', 'idToko', 'Garansi', 'Harga', 'Rangking', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
