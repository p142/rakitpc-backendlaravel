<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListToko extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'list_toko';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idToko';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'idToko',
        'NamaToko',
        'NamaPemilik',
        'Alamat',
        'NoTlp',
        'LinkSosmed'
    ];

    public function casing()
    {
        return $this->hasMany(casing::class, 'idToko');
    }

    use HasFactory;
}
