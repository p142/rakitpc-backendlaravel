<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\storage;

class v2StorageController extends Controller
{
    public function getStorage(){
        $hasil =  storage::select("*")
                  ->where('idStorage','!=' , 0)
                  ->where('Rangking', 1)
                  ->get();
                  
        return $hasil;
    }

    public function getStorageID($id){
        $hasil =  storage::select("*")
                        ->where('idStorage', $id)
                        ->get();
        return $hasil;
    }

    public function PostStorageFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = storage::select("*")
                        ->orderBy("NamaStorage")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = storage::select("*")
                        ->orderByDesc("NamaStorage")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = storage::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "SSD") {
            $hasil = storage::select("*")
                        ->where('TypeStorage','like','%ssd%')
                        ->get(); 
          }
          elseif ($Request->Request == "HDD") {
            $hasil = storage::select("*")
                        ->where('TypeStorage','like','%hdd%')
                        ->get(); 
          }
          elseif ($Request->Request == "NVME") {
            $hasil = storage::select("*")
                        ->where('TypeStorage','like','%nvme%')
                        ->get(); 
          }
          elseif ($Request->Request == "CapacityTinggi") {
            $hasil = storage::select("*")
                        ->orderByDesc("StorageCapacity")
                        ->get();
          }
          elseif ($Request->Request == "CapacityRendah") {
            $hasil = storage::select("*")
                        ->orderBy("StorageCapacity")
                        ->get();
          }
          elseif ($Request->Request == "ReadSpeed") {
            $hasil = storage::select("*")
                        ->orderByDesc("ReadSpeed")
                        ->get();
          }
          elseif ($Request->Request == "WriteSpeed") {
            $hasil = storage::select("*")
                        ->orderByDesc("WriteSpeed")
                        ->get();
          }else {
            $hasil =  $hasil =  storage::select("*")
            ->where('idStorage','!=' , 0)
            ->where('Rangking', 1)
            ->get();
          }
          return $hasil;
    }
}
