<div  
x-data="{ mode: 'light' }"
x-init="mode = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'"
x-on:dark-mode-toggled.window="mode = $event.detail"
>
    <span x-show="mode === 'light'">
        <img
            src="{{ asset('/images/logo-light.png') }}"
            alt="{{ env('APP_NAME') }} Light Logo"
            class="relative z-20 block h-10 w-full"
        />
    </span>
    
    <span x-show="mode === 'dark'">
        <img
            src="{{ asset('/images/logo-dark.png') }}"
            alt="{{ env('APP_NAME') }} Dark Logo"
            class="relative z-20 block h-10 w-full"
            />
    </span>
</div>
