<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\keyboard;

class KeyboardController extends Controller
{
   public function getKeyboard(){
      $hasil = keyboard::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
   }

   public function getKeyboardID($id){
      $hasil =  keyboard::select("*")
                  ->where('idKeyboard', $id)
                  ->get();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
   }

    public function PostKeyboardFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = keyboard::select("*")
                        ->orderBy("NamaKeyboard")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = keyboard::select("*")
                        ->orderByDesc("NamaKeyboard")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = keyboard::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = keyboard::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mechanical") {
            $hasil = keyboard::select("*")
                        ->where('Mechanical','like','%yes%')
                        ->get();
         }
         elseif ($Request->Request == "Blue") {
            $hasil = keyboard::select("*")
                        ->where('SwitchType','like','%blue%')
                        ->get();
         }
         elseif ($Request->Request == "Brown") {
            $hasil = keyboard::select("*")
                        ->where('SwitchType','like','%brown%')
                        ->get();
         }
         elseif ($Request->Request == "Red") {
            $hasil = keyboard::select("*")
                        ->where('SwitchType','like','%red%')
                        ->get();
         }
         elseif ($Request->Request == "RGB") {
            $hasil = keyboard::select("*")
                        ->where('RGB','like','%yes%')
                        ->get();
         }
         elseif ($Request->Request == "Wireless") {
            $hasil = keyboard::select("*")
                        ->where('Wireless','like','%yes%')
                        ->get();
         }
         elseif ($Request->Request == "Full") {
            $hasil = keyboard::select("*")
                        ->where('KeyboardType','like','%full%')
                        ->get();
         }
         elseif ($Request->Request == "TKL") {
            $hasil = keyboard::select("*")
                        ->where('KeyboardType','like','%tenkey%')
                        ->get();
         }
         elseif ($Request->Request == "80") {
            $hasil = keyboard::select("*")
                        ->where('KeyboardType','like','%80%')
                        ->get();
         }
         elseif ($Request->Request == "65") {
            $hasil = keyboard::select("*")
                        ->where('KeyboardType','like','%65%')
                        ->get();
         }
         else {
            $hasil = keyboard::all();
         }
         $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
         return $hasil;
    }
}
