<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\buildsNew;
use DateTimeInterface;

class motherboard extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'motherboard';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMotherboard';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaMobo', 'MerkMobo', 'FormFactor', 'SocketMobo', 'ChipsetMobo', 'MemoryType', 'SlotMemory', 'SataSlot', 'PCIE', 'PCIgen', 'M2Slot', 'UsbPort', 'AudioPort', 'DisplayOutput', 'LanPort', 'ImageLink', 'Warna', 'RGB', 'idToko', 'Rangking', 'Garansi', 'Harga', 'Links', 'created_at', 'updated_at'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
