<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class keyboard extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'keyboard';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idKeyboard';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaKeyboard', 'MerkKeyboard', 'Mechanical', 'SwitchType', 'KeyboardType', 'Wireless', 'RGB', 'ImageLink', 'KeyboardColor', 'idToko', 'Garansi', 'Harga', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
