<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class cpuCooler extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cpu_cooler';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idCooler';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaCooler', 'MerkCooler', 'TypeCooler', 'SocketCooler', 'DimensionCooler', 'FanQuantity', 'FanSpeed', 'PowerCooler', 'ColorCooler', 'RGB', 'idToko', 'Garansi', 'Harga', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
