<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cpuCooler;

class CpuCoolerController extends Controller
{
    public function getCpuCooler(){
      $hasil = cpuCooler::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }

    public function getCpuCoolerID($id){
        $hasil =  cpuCooler::select("*")
                        ->where('idCooler', $id)
                        ->get();

        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }

    public function PostCpuCoolerFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = cpuCooler::select("*")
                        ->orderBy("NamaCooler")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = cpuCooler::select("*")
                        ->orderByDesc("NamaCooler")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = cpuCooler::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif (($Request->Request) == "Mahal") {
            $hasil = cpuCooler::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Intel") {
            $hasil = cpuCooler::select("*")
                        ->where('SocketCooler','like','%LGA%')
                        ->get(); 
         }
         elseif ($Request->Request == "AMD") {
            $hasil = cpuCooler::select("*")
                        ->where('SocketCooler','like','%AM%')
                        ->get(); 
         }
         elseif ($Request->Request == "Air") {
            $hasil = cpuCooler::select("*")
                        ->where('TypeCooler','like','%Air%')
                        ->get(); 
         }
         elseif ($Request->Request == "Water") {
            $hasil = cpuCooler::select("*")
                        ->where('TypeCooler','like','%Liquid%')
                        ->get(); 
         }  
         else {
            $hasil = cpuCooler::all(); 
        }
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }
}
