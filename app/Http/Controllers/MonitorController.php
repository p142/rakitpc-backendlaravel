<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\monitor;

class MonitorController extends Controller
{
    public function getMonitor(){
      $hasil = monitor::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }

    public function getMonitorID($id){
        $hasil =  monitor::select("*")
                        ->where('idMonitor', $id)
                        ->get();
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }

    public function PostMonitorFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = monitor::select("*")
                        ->orderBy("NamaMonitor")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = monitor::select("*")
                        ->orderByDesc("NamaMonitor")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = monitor::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = monitor::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "1080p") {
            $hasil = monitor::select("*")
                        ->where('MonitorResolusi','like','%1080%')
                        ->get();
         }
         elseif ($Request->Request == "1440p") {
            $hasil = monitor::select("*")
                        ->where('MonitorResolusi','like','%1440%')
                        ->get();
         }
         elseif ($Request->Request == "2160p") {
            $hasil = monitor::select("*")
                        ->where('MonitorResolusi','like','%2160%')
                        ->get();
         }
         elseif ($Request->Request == "HertzTinggi") {
          $hasil = monitor::select("*")
                      ->orderByDesc("MonitorRefreshRate")
                      ->get();
         }
         elseif ($Request->Request == "HertzRendah") {
          $hasil = monitor::select("*")
                      ->orderBy("MonitorRefreshRate")
                      ->get();
         }
         elseif ($Request->Request == "Ultrawide") {
          $hasil = monitor::select("*")
                        ->where('Ultrawide','like','%yes%')
                        ->get();
         }
         elseif ($Request->Request == "HDR") {
          $hasil = monitor::select("*")
                        ->where('HDR','like','%hdr%')
                        ->get();
         }
         elseif ($Request->Request == "FreeSync") {
          $hasil = monitor::select("*")
                        ->where('ScreenTechnology','like','%free%')
                        ->get();
         }
         elseif ($Request->Request == "GSYNC") {
          $hasil = monitor::select("*")
                        ->where('ScreenTechnology','like','%g-sync%')
                        ->get();
         }
         else {
            $hasil = monitor::all();
         }
         $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
         return $hasil;
    }
}
