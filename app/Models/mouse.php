<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class mouse extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mouse';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMouse';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaMouse', 'MerkMouse', 'DpiMaxMouse', 'DpiMouse', 'MouseColor', 'SensorMouse', 'TotalButton', 'Weight', 'Wireless', 'ImageLinks', 'Links', 'idToko', 'Rangking', 'Garansi', 'Harga', 'RGB', 'created_at', 'updated_at'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
