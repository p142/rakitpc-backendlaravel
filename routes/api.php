
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CasingController;
use App\Http\Controllers\CpuController;
use App\Http\Controllers\CpuCoolerController;
use App\Http\Controllers\MotherboardController;
use App\Http\Controllers\PowerSupplyController;
use App\Http\Controllers\RamController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\VgaController;
use App\Http\Controllers\BuildsController;
use App\Http\Controllers\FanController;
use App\Http\Controllers\MonitorController;
use App\Http\Controllers\KeyboardController;
use App\Http\Controllers\MouseController;
use App\Http\Controllers\SimpanBuildController;
use App\Http\Controllers\V2\v2BuildsController;
use App\Http\Controllers\V2\v2CasingController;
use App\Http\Controllers\V2\v2CpuController;
use App\Http\Controllers\V2\v2CpuCoolerController;
use App\Http\Controllers\V2\v2FanController;
use App\Http\Controllers\V2\v2MotherboardController;
use App\Http\Controllers\V2\v2PowerSupplyController;
use App\Http\Controllers\V2\v2RamController;
use App\Http\Controllers\V2\v2StorageController;
use App\Http\Controllers\V2\v2KeyboardController;
use App\Http\Controllers\V2\v2MonitorController;
use App\Http\Controllers\V2\v2MouseController;
use App\Http\Controllers\V2\v2VgaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('v2/builds',[v2BuildsController::class,'getBuilds']);
Route::get('v2/builds/{id}',[v2BuildsController::class,'getBuildsID']);
Route::post('v2/builds/jawab',[v2BuildsController::class,'getBuildsDetail']);

Route::get('v2/casing',[v2CasingController::class,'getCasing']);
Route::post('v2/casing/Filter',[v2CasingController::class,'postCasingFilter']);
Route::get('v2/casing/{id}',[v2CasingController::class,'getCasingID']);

Route::get('v2/cpu',[v2CpuController::class,'getCpu']);
Route::post('v2/cpu/Filter',[v2CpuController::class,'postCpuFilter']);
Route::get('v2/cpu/{id}',[v2CpuController::class,'getCpuID']);

Route::get('v2/cpucooler',[v2CpuCoolerController::class,'getCpuCooler']);
Route::post('v2/cpucooler/Filter',[v2CpuCoolerController::class,'postCpuCoolerFilter']);
Route::get('v2/cpucooler/{id}',[v2CpuCoolerController::class,'getCpuCoolerID']);

Route::get('v2/fan',[v2FanController::class,'getFan']);
Route::post('v2/fan/Filter',[v2FanController::class,'postFanFilter']);
Route::get('v2/fan/{id}',[v2FanController::class,'getFanID']);

Route::get('v2/motherboard',[v2MotherboardController::class,'getMobo']);
Route::post('v2/motherboard/Filter',[v2MotherboardController::class,'postMoboFilter']);
Route::get('v2/motherboard/{id}',[v2MotherboardController::class,'getMoboID']);

Route::get('v2/psu',[v2PowerSupplyController::class,'getPowerSupply']);
Route::post('v2/psu/Filter',[v2PowerSupplyController::class,'postPowerSupplyFilter']);
Route::get('v2/psu/{id}',[v2PowerSupplyController::class,'getPowerSupplyID']);

Route::get('v2/ram',[v2RamController::class,'getRam']);
Route::post('v2/ram/Filter',[v2RamController::class,'postRamFilter']);
Route::get('v2/ram/{id}',[v2RamController::class,'getRamID']);

Route::get('v2/storage',[v2StorageController::class,'getStorage']);
Route::post('v2/storage/Filter',[v2StorageController::class,'postStorageFilter']);
Route::get('v2/storage/{id}',[v2StorageController::class,'getStorageID']);

Route::get('v2/vga',[v2VgaController::class,'getVga']);
Route::post('v2/vga/Filter',[v2VgaController::class,'postVgaFilter']);
Route::get('v2/vga/{id}',[v2VgaController::class,'getVgaID']);

Route::get('v2/monitor',[v2MonitorController::class,'getMonitor']);
Route::post('v2/monitor/Filter',[v2MonitorController::class,'PostMonitorFilter']);
Route::get('v2/monitor/{id}',[v2MonitorController::class,'getMonitorID']);

Route::get('v2/keyboard',[v2KeyboardController::class,'getKeyboard']);
Route::post('v2/keyboard/Filter',[v2KeyboardController::class,'PostKeyboardFilter']);
Route::get('v2/keyboard/{id}',[v2KeyboardController::class,'getKeyboardID']);

Route::get('v2/Mouse',[v2MouseController::class,'getMouse']);
Route::post('v2/Mouse/Filter',[v2MouseController::class,'PostMouseFilter']);
Route::get('v2/Mouse/{id}',[v2MouseController::class,'getMouseID']);

//V1 API
Route::get('Casing/All',[CasingController::class,'getCasing']);
Route::post('Casing/Filter',[CasingController::class,'postCasingFilter']);
Route::get('Casing/{id}',[CasingController::class,'getCasingID']);

Route::get('Cpu/All',[CpuController::class,'getCpu']);
Route::post('Cpu/Filter',[CpuController::class,'postCpuFilter']);
Route::get('Cpu/{id}',[CpuController::class,'getCpuID']);

Route::get('CpuCooler/All',[CpuCoolerController::class,'getCpuCooler']);
Route::post('CpuCooler/Filter',[CpuCoolerController::class,'postCpuCoolerFilter']);
Route::get('CpuCooler/{id}',[CpuCoolerController::class,'getCpuCoolerID']);

Route::get('Fan/All',[FanController::class,'getFan']);
Route::post('Fan/Filter',[FanController::class,'postFanFilter']);
Route::get('Fan/{id}',[FanController::class,'getFanID']);

Route::get('Motherboard/All',[MotherboardController::class,'getMobo']);
Route::post('Motherboard/Filter',[MotherboardController::class,'postMoboFilter']);
Route::get('Motherboard/{id}',[MotherboardController::class,'getMoboID']);

Route::get('Psu/All',[PowerSupplyController::class,'getPowerSupply']);
Route::post('Psu/Filter',[PowerSupplyController::class,'postPowerSupplyFilter']);
Route::get('Psu/{id}',[PowerSupplyController::class,'getPowerSupplyID']);

Route::get('Ram/All',[RamController::class,'getRam']);
Route::post('Ram/Filter',[RamController::class,'postRamFilter']);
Route::get('Ram/{id}',[RamController::class,'getRamID']);

Route::get('Storage/All',[StorageController::class,'getStorage']);
Route::post('Storage/Filter',[StorageController::class,'postStorageFilter']);
Route::get('Storage/{id}',[StorageController::class,'getStorageID']);

Route::get('Vga/All',[VgaController::class,'getVga']);
Route::post('Vga/Filter',[VgaController::class,'postVgaFilter']);
Route::get('Vga/{id}',[VgaController::class,'getVgaID']);

Route::get('Builds/All',[BuildsController::class,'getBuilds']);
Route::get('Builds/{id}',[BuildsController::class,'getBuildsID']);
Route::post('Builds/Detail',[BuildsController::class,'getBuildsDetail']);

Route::get('Monitor/All',[MonitorController::class,'getMonitor']);
Route::post('Monitor/Filter',[MonitorController::class,'PostMonitorFilter']);
Route::get('Monitor/{id}',[MonitorController::class,'getMonitorID']);

Route::get('Keyboard/All',[KeyboardController::class,'getKeyboard']);
Route::post('Keyboard/Filter',[KeyboardController::class,'PostKeyboardFilter']);
Route::get('Keyboard/{id}',[KeyboardController::class,'getKeyboardID']);

Route::get('Mouse/All',[MouseController::class,'getMouse']);
Route::post('Mouse/Filter',[MouseController::class,'PostMouseFilter']);
Route::get('Mouse/{id}',[MouseController::class,'getMouseID']);

Route::get('SimpanBuild/All',[SimpanBuildController::class,'getBuildAll']);
Route::post('SimpanBuild/Upload',[SimpanBuildController::class,'postSaveBuild']);
Route::get('SimpanBuild/User/{id}',[SimpanBuildController::class,'getBuildIDUser']);
Route::get('SimpanBuild/{id}',[SimpanBuildController::class,'getBuildIDBuild']);
Route::get('SimpanBuild/Delete/{id}',[SimpanBuildController::class,'getBuildDelete']);
Route::get('SimpanBuild/User/Day/{id}',[SimpanBuildController::class,'getBuildIDUser']);
Route::get('SimpanBuild/User/Month/{id}',[SimpanBuildController::class,'getBuildIDUser']);
Route::get('SimpanBuild/User/Year/{id}',[SimpanBuildController::class,'getBuildIDUser']);