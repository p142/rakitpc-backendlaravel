<?php

namespace App\Filament\Resources\CasingResource\Pages;

use App\Filament\Resources\CasingResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCasings extends ListRecords
{
    protected static string $resource = CasingResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
