<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vga;

class VgaController extends Controller
{
    public function getVga(){
      $hasil = Vga::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }

    public function getVgaV2(){
      $hasil =  Vga::select("*")
                  ->where('Rangking', 1)
                  ->get();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }


    public function getVgaID($id){
        $hasil =  Vga::select("*")
                        ->where('idVga', $id)
                        ->get();
        return $hasil;
    }

    public function PostVgaFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = vga::select("*")
                        ->orderBy("NamaVga")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = vga::select("*")
                        ->orderByDesc("NamaVga")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = vga::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "AMD") {
            $hasil = vga::select("*")
                        ->where('Generation','like','%AMD%')
                        ->get(); 
          }
          elseif ($Request->Request == "NVIDIA") {
            $hasil = vga::select("*")
                        ->where('NamaVga','like','%tx%')
                        ->get(); 
          }
          elseif ($Request->Request == "RTX") {
            $hasil = vga::select("*")
                        ->where('RTcores','like','%0%')
                        ->get(); 
          }
          elseif ($Request->Request == "Vramlebih4GB") {
            $hasil = vga::select("*")
                        ->where('MemoryVGA','like','%0%')
                        ->get(); 
          }
          else {
            $hasil = vga::all();
          }
          $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
          return $hasil;
    }
}
