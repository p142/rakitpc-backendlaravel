<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mouse;

class MouseController extends Controller
{
    public function getMouse(){
      $hasil = mouse::all();
      $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
      return $hasil;
    }

    public function getMouseID($id){
        $hasil =  mouse::select("*")
                        ->where('idMouse', $id)
                        ->get();
        $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
        return $hasil;
    }

    public function PostmouseFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = mouse::select("*")
                        ->orderBy("NamaMouse")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = mouse::select("*")
                        ->orderByDesc("NamaMouse")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = mouse::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = mouse::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "DpiTerbanyak") {
            $hasil = mouse::select("*")
                        ->orderByDesc("DpiMaxMouse")
                        ->get();
         }
         elseif ($Request->Request == "ButtonTerbanyak") {
            $hasil = mouse::select("*")
                        ->orderByDesc("TotalButton")
                        ->get();
         }
         elseif ($Request->Request == "Teringan") {
            $hasil = mouse::select("*")
                        ->orderBy("Weight")
                        ->get();
         }
         elseif ($Request->Request == "Wireless") {
            $hasil = mouse::select("*")
                        ->where('Wireless','like','%yes%')
                        ->get();
         }
         else {
            $hasil = mouse::all();
         }
         $hasil->makeHidden(['Toko','Garansi','Rangking', 'created_at', 'updated_at']);
         return $hasil;
    }
}
