<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\buildsNew;

class v2BuildsController extends Controller
{
    public function getBuilds(){
        $hasil = buildsNew::join('motherboard', 'motherboard.idMotherboard', '=', 'builds_new.Motherboard')
                            ->join('cpu', 'cpu.idCpu', '=', 'builds_new.Cpu')
                            ->join('ram', 'ram.idRam', '=', 'builds_new.Ram')
                            ->join('vga', 'vga.idVGA', '=', 'builds_new.vga')
                            ->join('power_supply', 'power_supply.idPsu', '=', 'builds_new.Psu')
                            ->join('cpu_cooler', 'cpu_cooler.idCooler', '=', 'builds_new.CpuCooler')
                            ->join('storage AS storage1', 'storage1.idStorage', '=', 'builds_new.Storage')
                            ->join('storage AS storage2', 'storage2.idStorage', '=', 'builds_new.Storage2')
                            ->join('fan', 'fan.idFans', '=', 'builds_new.Fans')
                            ->join('casing', 'casing.idCasing', '=', 'builds_new.Casing')
                            ->get(['builds_new.idBuilds','builds_new.Toko','builds_new.Rangking','builds_new.Garansi', 'builds_new.HargaBuilds', 
                                    'motherboard.NamaMobo', 'motherboard.Harga AS HargaMobo','motherboard.ImageLink AS ImgMobo',    
                                    'builds_new.Cpu', 'cpu.NamaCpu', 'cpu.Harga AS HargaCpu','cpu.ImageLink AS ImgCpu',
                                    'builds_new.Ram', 'ram.NamaRam', 'ram.Harga AS HargaRam','ram.ImageLink AS ImgRam', 
                                    'builds_new.Vga', 'vga.NamaVga', 'vga.Harga AS HargaVga','vga.ImageLink AS ImgVga', 
                                    'builds_new.Psu', 'power_supply.NamaPsu', 'power_supply.Harga AS HargaPsu','power_supply.ImageLink AS ImgPsu',
                                    'builds_new.CpuCooler', 'cpu_cooler.NamaCooler', 'cpu_cooler.Harga AS HargaCpuCooler','cpu_cooler.ImageLink AS ImgCpuCooler',
                                    'builds_new.Storage', 'storage1.NamaStorage', 'storage1.Harga AS HargaStorage','storage1.ImageLink AS ImgStorage',
                                    'builds_new.Storage2', 'storage2.NamaStorage AS NamaStorage2', 'storage2.Harga AS HargaStorage2','storage2.ImageLink AS ImgStorage2',
                                    'builds_new.Fans', 'fan.NamaFans', 'fan.Harga AS HargaFans','fan.ImageLinks AS ImgFans',
                                    'builds_new.Casing', 'casing.NamaCasing', 'casing.Harga AS HargaCasing','casing.ImageLink AS ImgCasing']);
        return $hasil;
    }

    public function getBuildsID($id){
        $hasil = buildsNew::join('motherboard', 'motherboard.idMotherboard', '=', 'builds_new.Motherboard')
                            ->join('cpu', 'cpu.idCpu', '=', 'builds_new.Cpu')
                            ->join('ram', 'ram.idRam', '=', 'builds_new.Ram')
                            ->join('vga', 'vga.idVGA', '=', 'builds_new.vga')
                            ->join('power_supply', 'power_supply.idPsu', '=', 'builds_new.Psu')
                            ->join('cpu_cooler', 'cpu_cooler.idCooler', '=', 'builds_new.CpuCooler')
                            ->join('storage AS storage1', 'storage1.idStorage', '=', 'builds_new.Storage')
                            ->join('storage AS storage2', 'storage2.idStorage', '=', 'builds_new.Storage2')
                            ->join('fan', 'fan.idFans', '=', 'builds_new.Fans')
                            ->join('casing', 'casing.idCasing', '=', 'builds_new.Casing')
                            ->where('idBuilds', $id)
                            ->get(['builds_new.idBuilds','builds_new.Toko','builds_new.Rangking','builds_new.Garansi', 'builds_new.HargaBuilds', 
                                    'motherboard.NamaMobo', 'motherboard.Harga AS HargaMobo','motherboard.ImageLink AS ImgMobo',    
                                    'builds_new.Cpu', 'cpu.NamaCpu', 'cpu.Harga AS HargaCpu','cpu.ImageLink AS ImgCpu',
                                    'builds_new.Ram', 'ram.NamaRam', 'ram.Harga AS HargaRam','ram.ImageLink AS ImgRam', 
                                    'builds_new.Vga', 'vga.NamaVga', 'vga.Harga AS HargaVga','vga.ImageLink AS ImgVga', 
                                    'builds_new.Psu', 'power_supply.NamaPsu', 'power_supply.Harga AS HargaPsu','power_supply.ImageLink AS ImgPsu',
                                    'builds_new.CpuCooler', 'cpu_cooler.NamaCooler', 'cpu_cooler.Harga AS HargaCpuCooler','cpu_cooler.ImageLink AS ImgCpuCooler',
                                    'builds_new.Storage', 'storage1.NamaStorage', 'storage1.Harga AS HargaStorage','storage1.ImageLink AS ImgStorage',
                                    'builds_new.Storage2', 'storage2.NamaStorage AS NamaStorage2', 'storage2.Harga AS HargaStorage2','storage2.ImageLink AS ImgStorage2',
                                    'builds_new.Fans', 'fan.NamaFans', 'fan.Harga AS HargaFans','fan.ImageLinks AS ImgFans',
                                    'builds_new.Casing', 'casing.NamaCasing', 'casing.Harga AS HargaCasing','casing.ImageLink AS ImgCasing']);
        return $hasil;
    }

    public function getBuildsDetail(Request $Request){
        $hasil = buildsNew::join('motherboard', 'motherboard.idMotherboard', '=', 'builds_new.Motherboard')
                            ->join('cpu', 'cpu.idCpu', '=', 'builds_new.Cpu')
                            ->join('ram', 'ram.idRam', '=', 'builds_new.Ram')
                            ->join('vga', 'vga.idVGA', '=', 'builds_new.vga')
                            ->join('power_supply', 'power_supply.idPsu', '=', 'builds_new.Psu')
                            ->join('cpu_cooler', 'cpu_cooler.idCooler', '=', 'builds_new.CpuCooler')
                            ->join('storage AS storage1', 'storage1.idStorage', '=', 'builds_new.Storage')
                            ->join('storage AS storage2', 'storage2.idStorage', '=', 'builds_new.Storage2')
                            ->join('fan', 'fan.idFans', '=', 'builds_new.Fans')
                            ->join('casing', 'casing.idCasing', '=', 'builds_new.Casing')
                            ->where('KebutuhanBuilds', $Request->Kebutuhan)
                            ->where('BudgetBuilds', $Request->Budget)
                            ->where('KebutuhanStorage', $Request->Storage)
                            ->where('Rangking', 1)
                            ->get(['builds_new.idBuilds','builds_new.Toko','builds_new.Rangking','builds_new.Garansi', 'builds_new.HargaBuilds', 
                                    'motherboard.NamaMobo', 'motherboard.Harga AS HargaMobo','motherboard.ImageLink AS ImgMobo',    
                                    'builds_new.Cpu', 'cpu.NamaCpu', 'cpu.Harga AS HargaCpu','cpu.ImageLink AS ImgCpu',
                                    'builds_new.Ram', 'ram.NamaRam', 'ram.Harga AS HargaRam','ram.ImageLink AS ImgRam', 
                                    'builds_new.Vga', 'vga.NamaVga', 'vga.Harga AS HargaVga','vga.ImageLink AS ImgVga', 
                                    'builds_new.Psu', 'power_supply.NamaPsu', 'power_supply.Harga AS HargaPsu','power_supply.ImageLink AS ImgPsu',
                                    'builds_new.CpuCooler', 'cpu_cooler.NamaCooler', 'cpu_cooler.Harga AS HargaCpuCooler','cpu_cooler.ImageLink AS ImgCpuCooler',
                                    'builds_new.Storage', 'storage1.NamaStorage', 'storage1.Harga AS HargaStorage','storage1.ImageLink AS ImgStorage',
                                    'builds_new.Storage2', 'storage2.NamaStorage AS NamaStorage2', 'storage2.Harga AS HargaStorage2','storage2.ImageLink AS ImgStorage2',
                                    'builds_new.Fans', 'fan.NamaFans', 'fan.Harga AS HargaFans','fan.ImageLinks AS ImgFans',
                                    'builds_new.Casing', 'casing.NamaCasing', 'casing.Harga AS HargaCasing','casing.ImageLink AS ImgCasing']);
                                            
        return $hasil;
    }
}
