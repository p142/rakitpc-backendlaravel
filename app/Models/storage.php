<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class storage extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'storage';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idStorage';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaStorage', 'MerkStorage', 'TypeStorage', 'FormFactor', 'StorageCapacity', 'StorageInterface', 'Cache', 'ReadSpeed', 'WriteSpeed', 'RPM', 'StorageWatt', 'idToko', 'Garansi', 'Harga', 'ImageLink', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
