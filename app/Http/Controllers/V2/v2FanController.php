<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\fan;

class v2FanController extends Controller
{
    public function getFan(){
        $hasil = fan::select("*")
                ->where('Rangking', 1)
                ->get();

        return $hasil;
    }

    public function getFanID($id){
        $hasil =  fan::select("*")
                        ->where('idFans', $id)
                        ->get();
        return $hasil;
    }

    public function PostFanFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = fan::select("*")
                        ->orderBy("NamaFans")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = fan::select("*")
                        ->orderByDesc("NamaFans")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = fan::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = fan::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Big") {
            $hasil = fan::select("*")
                        ->orderByDesc("SizeFans")
                        ->get();
        }
        elseif ($Request->Request == "Small") {
            $hasil = fan::select("*")
                        ->orderBy("SizeFans")
                        ->get();
        }
        elseif ($Request->Request == "RGB") {
            $hasil = fan::select("*")
                        ->where('RGB','like','%YES%')
                        ->get(); 
        }
        else {
            $hasil = fan::select("*")
                        ->where('Rangking', 1)
                        ->get();
        }
        return $hasil;
    }
}
