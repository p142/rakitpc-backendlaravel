<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class vga extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'vga';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idVGA';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['NamaVGA', 'ReleaseDate', 'MerkVGA', 'Generation', 'Interface', 'BaseClocks', 'BoostClock', 'MemoryClock', 'MemoryVGA', 'MemoryType', 'MemoryBus', 'OutputPort', 'PowerConsumption', 'PowerConnection', 'DimensionVGA', 'Architecture', 'MaxDisplay', 'RGB', 'RTcores', 'Color', 'ImageLink', 'idToko', 'Garansi', 'Harga', 'GraphicAPI', 'DisplayTechnology', 'Links'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
