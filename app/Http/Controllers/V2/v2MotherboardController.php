<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\motherboard;

class v2MotherboardController extends Controller
{
    public function getMobo(){
        $hasil = motherboard::select("*")
                ->where('Rangking', 1)
                ->get();
        return $hasil;
    }

    public function getMoboID($id){
        $hasil =  motherboard::select("*")
                        ->where('idMotherboard', $id)
                        ->get();
        return $hasil;
    }

    public function PostMoboFilter(Request $Request){
        if ($Request->Request == "AZ"){
            $hasil = motherboard::select("*")
                        ->orderBy("NamaMobo")
                        ->get();
          }
          elseif ($Request->Request == "ZA") {
            $hasil = motherboard::select("*")
                        ->orderByDesc("NamaMobo")
                        ->get();
          }
          elseif ($Request->Request == "Murah") {
            $hasil = motherboard::select("*")
                        ->orderBy("Harga")
                        ->get();
          }
          elseif ($Request->Request == "Mahal") {
            $hasil = motherboard::select("*")
                        ->orderByDesc("Harga")
                        ->get();
          }
          elseif ($Request->Request == "AMD") {
            $hasil = motherboard::select("*")
                        ->where('SocketMobo','like','%AM%')
                        ->get(); 
        }
        elseif ($Request->Request == "Intel") {
            $hasil = motherboard::select("*")
                        ->where('SocketMobo','like','%LGA%')
                        ->get(); 
        }
        elseif ($Request->Request == "NVME") {
            $hasil = motherboard::select("*")
                        ->where('M2Slot','like','%x%')
                        ->get(); 
        }
        elseif ($Request->Request == "ATX") {
            $hasil = motherboard::select("*")
                        ->where('FormFactor','like','%ATX%')
                        ->get(); 
        }
        elseif ($Request->Request == "Micro") {
            $hasil = motherboard::select("*")
            ->where('FormFactor','like','%Micro%')
            ->get(); 
        }
        elseif ($Request->Request == "Mini") {
            $hasil = motherboard::select("*")
            ->where('FormFactor','like','%Mini%')
            ->get(); 
        }
        else {
            $hasil = motherboard::select("*")
                    ->where('Rangking', 1)
                    ->get();
        }
        return $hasil;
    }
}
