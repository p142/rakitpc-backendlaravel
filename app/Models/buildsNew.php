<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\motherboard;
use DateTimeInterface;

class buildsNew extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'builds_new';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idBuilds';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['KebutuhanBuilds', 'BudgetBuilds', 'KebutuhanStorage', 'idToko', 'Rangking', 'Garansi', 'Motherboard', 'Cpu', 'Ram', 'Vga', 'PSU', 'CpuCooler', 'Storage', 'Storage2', 'Fans', 'Casing', 'ImgLinks', 'HargaBuilds'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    use HasFactory;
}
