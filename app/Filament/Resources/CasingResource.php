<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CasingResource\Pages;
use App\Filament\Resources\CasingResource\RelationManagers;
use App\Models\Casing;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Filters\SelectFilter;

class CasingResource extends Resource
{
    protected static ?string $model = Casing::class;

    protected static ?string $navigationIcon = 'si-pcgamingwiki';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Fieldset::make()->schema([
                    Grid::make('full')->schema([
                        TextInput::make(name: 'NamaCasing')->required()->label('Nama')->autofocus(),
                        TextInput::make(name: 'MerkCasing')->required()->label('Merk'),
                    ]),
                ]),

                Fieldset::make('Deskripsi Casing')->schema([
                    Grid::make(2)->schema([
                        TextInput::make(name: 'MoboCompatible')->required()->label('Mobo Compatible'),
                        TextInput::make(name: 'DrivebayCasing')->required()->label('Drive Bay Casing'),
                        TextInput::make(name: 'FanSupport')->required()->label('Fan Support'),
                        TextInput::make(name: 'FrontPanel')->required()->label('Front Panel'),
                        TextInput::make(name: 'DimensionCasing')->required()->label('Dimension Casing'),
                        TextInput::make(name: 'WeightCasing')->required()->label('Weight Casing'),
                        TextInput::make(name: 'ColorCasing')->required()->label('Color Casing'),
                        TextInput::make(name: 'MaxVgaLength')->required()->label('Max VGA Length'),
                        TextInput::make(name: 'MaxCoolerHeight')->required()->label('Max Cooler Height'),
                        TextInput::make(name: 'MaxPSU')->required()->label('Max PSU'),
                        TextInput::make(name: 'CasingSidePanel')->required()->label('Casing Side Panel'),
                        TextInput::make(name: 'Harga')->required()->numeric()
                            ->mask(fn (TextInput\Mask $mask) => $mask->money(prefix: 'Rp', thousandsSeparator: '.', decimalPlaces: 2, isSigned: false)),
                    ]),
                ]),

                Fieldset::make('Keterangan')->schema([
                    Grid::make(3)->schema([
                        Select::make('idToko')
                            ->relationship('toko', 'NamaToko')
                            ->options(
                                Casing::all()->pluck('Toko.NamaToko', 'idToko'))
                            ->required()
                            ->default(0)
                            ->searchable()
                            ->label('Nama Toko'),
                        TextInput::make(name: 'Garansi')->required()->default('NULL'),
                        TextInput::make(name: 'Rangking')->required()->default(0),
                    ]),
                ]),

                Fieldset::make('Link')->schema([
                    Grid::make('full')->schema([
                        TextInput::make(name: 'ImageLink')->required()->url()->label('Image Link'),
                        TextInput::make(name: 'Links')->required()->url(),
                    ]),
                ]),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('idCasing')->sortable()->searchable()->label('ID'),
                ImageColumn::make('ImageLink')->size(80)->label('Image'),
                TextColumn::make('NamaCasing')->sortable()->searchable()->label('Nama'),
                TextColumn::make('MerkCasing')->sortable()->searchable()->label('Merk'),
                TextColumn::make('Harga')->money('IDR', true)->sortable()->searchable(),
                TextColumn::make('toko.NamaToko')->sortable()->searchable()->label('Nama Toko'),
                TextColumn::make('Garansi')->sortable(),
                TextColumn::make('Rangking')->sortable(),
                TextColumn::make('MoboCompatible')->sortable()->label('Mobo Compatible'),
                TextColumn::make('DrivebayCasing')->sortable()->label('Drive Bay Casing'),
                TextColumn::make('FanSupport')->sortable()->label('Fan Support'),
                TextColumn::make('FrontPanel')->sortable()->label('Front Panel'),
                TextColumn::make('DimensionCasing')->sortable()->label('Dimension Casing'),
                TextColumn::make('WeightCasing')->sortable()->label('Weight Casing'),
                TextColumn::make('ColorCasing')->sortable()->label('Color Casing'),
                TextColumn::make('MaxVgaLength')->sortable()->label('Max VGA Length'),
                TextColumn::make('MaxCoolerHeight')->sortable()->label('Max Cooler Height'),
                TextColumn::make('MaxPSU')->sortable()->label('Max PSU'),
                TextColumn::make('CasingSidePanel')->sortable()->label('Casing Side Panel'),
                TextColumn::make('Links')->copyable(),
            ])
            ->filters([
                SelectFilter::make('MerkCasing')->options(
                    Casing::pluck('MerkCasing', 'MerkCasing')
                )->label('Filter berdasarkan Merk Casing'),
                SelectFilter::make('idToko')->relationship('toko', 'NamaToko')->label('Filter berdasarkan Toko'),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\ReplicateAction::make()->label('Copy'),
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCasings::route('/'),
            'create' => Pages\CreateCasing::route('/create'),
            'edit' => Pages\EditCasing::route('/{record}/edit'),
        ];
    }

    protected static function getNavigationBadge(): ?string
    {
        return self::$model::count();
    }
}