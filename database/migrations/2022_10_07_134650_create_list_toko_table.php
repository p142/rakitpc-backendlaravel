<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListTokoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_toko', function (Blueprint $table) {
            $table->bigIncrements('idToko');
            $table->string('NamaToko',200);
            $table->string('NamaPemilik',200);
            $table->string('Alamat',200);
            $table->integer('NoTlp');
            $table->string('LinkSosmed',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_toko');
    }
}